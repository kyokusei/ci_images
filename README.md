# ci_images

Some personal GitLab CI images.

- `rust_gba` is for GBA development in general.
- `sass_mdbook` is for websites that use plain HTML, Sass, and mdbook.
- `web_book` is for websites that use plain HTML, Sass, and mdbook for websites, and can generate PDFs/EPUBs.
